![Ligarba](https://gitlab.com/bluedevil/ligarba/-/raw/master/art/LigarbaBaslik.png)

# Ligarba - Kesme Tanımlayıcı Tablosu Çözümleyici

Ligarba linux için bir karakter aygıtıdır. Ligarba projesi bir çekirdek modülü ve bir tetikleyici uygulamadan oluşur. Karakter aygıtı sisteme yüklendikten sonra tetikleyici uygulama çalıştırılarak IDT(kesme Tanımlayıcı Tablosu) çekirdekten kullanıcı uzayına alınır ve çözümlenip ekrana basılır.

```
$ sudo ./tetik /dev/ligarba /boot/System.map
                         _                        _                    
       .:/+++/:-`       (_ ) _                   ( )                   
    `/s          o:`     | |(_)   __     _ _ _ __| |_      _ _         
   /  \ /\  /      s-    | || | /'_ `\ /'_` | '__) '_`\  /'_` )     
 `o  <oddbbd    O    -   | || |( (_) |( (_| | |  | |_) )( (_| |        
 +    ddoodb>    o   o  (___|_)`\__  |`\__,_|_)  (_,__/'`\__,_)      
 s   <dhqpdo   o     s         ( )_) |                                 
 +   /  \/ \         +          \___/'                               
 .     o            s` Yaylalari daglari da ligarbalar mi sardi...     
  -s  O            o`  Cekip giden sen idun da gene suc bana kaldi..   
   `/s  o        o-    Oy yaylalar yaylalar da tirpan cimen kucaklar.. 
      `-/++++/:-`      Yuregi yanan var mi da benum gibi usaklar...    
         _____________________________________________________        
      -+| @yazar    : Blue DeviL <bluedevil@sctzine.com>      |+-     
 _   |  | @surum    : 0.1.0                                   |  |   _
 _-==+--| @tarih    : 24/12/2020                              |--+==-_
     |  | @lisans   : MIT                                     |  |    
      -+| @bilgi    : IDT Parser              www.sctzine.com |+-     
        |_____________________________________________________|       

[int]*[      adres       ]*[ segment ]*[DPL]*[     Type     ]*[   yordam adi   ]
[0  ]  0XFFFFFFFF816A8120   KERNEL_CS    0    KESME KAPISI     divide_error
[1  ]  0XFFFFFFFF8169EDB0   KERNEL_CS    0    KESME KAPISI     debug
[2  ]  0XFFFFFFFF8169F1C0   KERNEL_CS    0    KESME KAPISI     nmi
[3  ]  0XFFFFFFFF8169EDF0   KERNEL_CS    3    KESME KAPISI     int3
[4  ]  0XFFFFFFFF816A8140   KERNEL_CS    3    KESME KAPISI     overflow
[5  ]  0XFFFFFFFF816A8160   KERNEL_CS    0    KESME KAPISI     bounds
[6  ]  0XFFFFFFFF816A8180   KERNEL_CS    0    KESME KAPISI     invalid_op
[7  ]  0XFFFFFFFF816A81A0   KERNEL_CS    0    KESME KAPISI     device_not_avail
[8  ]  0XFFFFFFFF816A81C0   KERNEL_CS    0    KESME KAPISI     double_fault
[9  ]  0XFFFFFFFF816A81F0   KERNEL_CS    0    KESME KAPISI     coprocessor_segm
[10 ]  0XFFFFFFFF816A8210   KERNEL_CS    0    KESME KAPISI     invalid_TSS
[11 ]  0XFFFFFFFF816A8240   KERNEL_CS    0    KESME KAPISI     segment_not_pres
[12 ]  0XFFFFFFFF8169EE30   KERNEL_CS    0    KESME KAPISI     stack_segment
[13 ]  0XFFFFFFFF8169EED0   KERNEL_CS    0    KESME KAPISI     general_protecti
[14 ]  0XFFFFFFFF8169EF00   KERNEL_CS    0    KESME KAPISI     page_fault
[15 ]  0XFFFFFFFF816A8270   KERNEL_CS    0    KESME KAPISI     spurious_interru
[16 ]  0XFFFFFFFF816A8290   KERNEL_CS    0    KESME KAPISI     coprocessor_erro
[17 ]  0XFFFFFFFF816A82B0   KERNEL_CS    0    KESME KAPISI     alignment_check
[18 ]  0XFFFFFFFF8169EF60   KERNEL_CS    0    KESME KAPISI     machine_check
[19 ]  0XFFFFFFFF816A82E0   KERNEL_CS    0    KESME KAPISI     simd_coprocessor
[20 ]  0XFFFFFFFF81CF30B4   KERNEL_CS    0    KESME KAPISI     bulunamadi
[21 ]  0XFFFFFFFF81CF30BD   KERNEL_CS    0    KESME KAPISI     bulunamadi
[22 ]  0XFFFFFFFF81CF30C6   KERNEL_CS    0    KESME KAPISI     bulunamadi
<kesinti>
```

## Derleme
Bu proje deneme amaçlı olarak Linux 3.5.0 çekirdeğini kullanmaktadır. Karakter aygıtını derlemek için bir `Makefile` vardır. Karakter aygıtını derlemek için:
```bash
$ make
```

Tetikleyici uygulamayı derlemek için gcc kullanıyoruz. Uygulama içerisinde `asm/segment.h` başlık dosyasını bulabilsin diye  `-I` parametresi ile bu başlık ve onun da bağlısı bir diğer başlığın yolu eklenmiştir.
```bash
gcc -ggdb -Wall -I/lib/modules/3.5.0-23-generic/build/arch/x86/include -I/lib/modules/3.5.0-23-generic/build/include tetik.c -o tetik
```

## Kullanım
Karakter aygıtı `insmod` ile sisteme yüklenebilir:
```bash
$ sudo insmod ./ligarba.ko
```

Tetikleyici uygulama parametre olarak karakter aygıtının yolunu ve sembolleri bulmak için `System.map` dosyasının yolunu istiyor. Uygulamayı sudo ile çalıştırmamın nedeni benim durumumda `System.map` dosyasının okunması için `kök kullanıcı` izinleri istenmesiydi.
```bash
$ sudo ./tetik /dev/ligarba /boot/System.map
```

## Yapılacaklar
Uygulama çekirdek 3.5.0 için hazırlandı. Güncel sürümlerle de çalışacak duruma getir.

## Yazarlar

* **Blue DeviL** - *Reverser* - [bluedevil](http://gitlab.com/bluedevil)

**HomePage** - [SCTZine](http://www.sctzine.com)

## Lisans

This project is under the MIT License.
