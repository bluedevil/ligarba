#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>       // file yapısı burada tanımlı
#include <linux/device.h>
#include <linux/uaccess.h>  // copy_[to|from]_user burada tanımlı

/**
 * /dev dizini altına girecek karakter aygıtı adı
 */
#define DEVICE_NAME "ligarba"


/**
 * yordam protipleri
 */
static int device_open(struct inode *i_node, struct file *fp);
static int device_release(struct inode *i_node, struct file *fp);
static ssize_t device_read(struct file *fp, char *buf, size_t count,
                           loff_t *off_t);
static ssize_t device_write(struct file *fp, const char *buf, size_t count,
                            loff_t *off_t);
static void idtr_bilgisi_al(void);

/**
 * değişkenler
 */
static int kapi_no = 0;
static int major_no = 0;
static int aygit_acildi_mi;
static struct class *class;

static struct file_operations fops = {
    .open       = device_open,
    .release    = device_release,
    .read       = device_read,
    .write      = device_write
};

static struct {
    uint16_t limit;
    uint64_t addr;
}__attribute__((__packed__)) idtr;

/*
 * zaten burada tanımlı:
 * /include/asm/desc_defs.h
 *
struct gate_struct64 {
    u16 offset_low;
    u16 segment;
    unsigned ist : 3, zero0 : 5, type : 5, dpl : 2, p : 1;
    u16 offset_middle;
    u32 offset_high;
    u32 zero1;
} __attribute__((packed)) gate;
*/
static struct gate_struct64 *gate;

/**
 * çekirdek modülü giriş yordamı
 */
static int __init driver_load(void)
{
    printk(KERN_INFO "[+] LOADED : char dev idt!\n");

    major_no = register_chrdev(0, DEVICE_NAME, &fops);
    if (major_no == 0){
        printk(KERN_INFO "[-] ERROR: register_chrdev!!\n");
    }else{
        printk(KERN_INFO "[+] major no = 0x%X\n", major_no);
    }
    
    class = class_create(THIS_MODULE, DEVICE_NAME);
    device_create(class, NULL, MKDEV(major_no, 0), NULL, DEVICE_NAME);

    return 0;
}

/**
 * çekirdek modülü çıkış ve temizlik yordamı
 */
static void __exit driver_unload(void)
{
    device_destroy(class, MKDEV(major_no, 0));
    class_unregister(class);
    class_destroy(class);

    unregister_chrdev(major_no, DEVICE_NAME);
    printk(KERN_INFO "[-] UNLOADED : char dev idt!\n");
    return;
}

/**
 * idtr_bilgisi_al -kesme tanımlayıcı tablosunun göstergecinin bulunduğu yazmacı
 * sidt intel komutu ile alır ve `idtr` yapısında saklar.
 */
static void idtr_bilgisi_al(void)
{
    asm volatile("sidt %0" : "=m" (idtr));
    gate = (struct gate_struct64 *)idtr.addr;
    printk(KERN_INFO "[GGA] gate: %lX\tsizeof: %ld\n",
           (long unsigned int)gate, sizeof(gate));
    return;
}
/**
 * device_open - kullanıcı uzayından ` open ` sistem çağrısı ile karakter aygıtı
 * okunmak istendiğinde çalışacak olan yordam.
 *
 * Peki çekirdek bunu nasıl biliyor sorusunun yanıtı ise `file_operations` adınd
 * aki çekirdekte tanımlı olan yapı sayesinde. Yukarıda bu yapıyı tanımladık  ve
 * bu yordamı ona argüman olarak verdik.
 */
static int device_open(struct inode *i_node, struct file *fp)
{
    // aygıtın iki kere açılıp hataya düşmemesi için `aygit_acildi_mi` değişkeni
    // ile bunu denetliyoruz.
    if (aygit_acildi_mi){
        return -EBUSY;
    }
    aygit_acildi_mi++;

    printk(KERN_INFO "[+] OPENED : char dev idt!\n");
    return 0;
}

/**
 * device_release - aygıt kapanırken çalışacak olan yordam. Burada salmak istedi
 * ğimiz dinamik değişkenleri, yapıları vb. salabilir,  sıfırlamak temizlemek ya
 * da ilk değerine döndürmek istediğmiz ne varsa -aygıtın bir sonraki kullanımın
 * a hazır hale getirilmesi için- yapılabilir.
 */
static int device_release(struct inode *i_node, struct file *fp)
{
    aygit_acildi_mi--;
    printk(KERN_INFO "[-] RELEASED : char dev idt!!\n");
    return 0;
}

/**
 * device_read - kullanıcı uzayından gelen `read` sistem çağrısını ele alan yord
 * am. Bu yazdığımız bir karakter aygıtı ve kullanıcı tarafından özellikle okuma
 * ve yazma isteği gelebilir.Bu yordam okuma yapılmak istendiğinde`copy_to_user`
 * ile kullanıcı uzayına okuduğu bilgiyi gönderir.
 */
static ssize_t device_read(struct file *fp, char *buf, size_t count,
                           loff_t *off_t)
{
    // gate yapisini kuralim
    idtr_bilgisi_al();
    if (copy_to_user(buf, gate, (0xFF * sizeof(struct gate_struct64))) != 0){
        printk(KERN_INFO "[-] ERROR: copy_to_user\n");
    }
    
    return kapi_no;
}

/**
 * device_write - kullanıcı uzayından karater aygıtımıza `write`  sistem çağrısı
 * ile veri yazdırılmak istendiğinde, bu isteği karşılan yordamdır. Bunu karşıla
 * mak için yordam içerisinde `copy_from_user` yordamını kullanıyoruz.
 */
static ssize_t device_write(struct file *fp, const char *buf, size_t count,
                            loff_t *off_t)
{
    if (copy_from_user(&kapi_no, buf, count) != 0){
        printk(KERN_INFO "[-] ERROR : copy_from_user\n");
    }
    return 0;
}


module_init(driver_load);
module_exit(driver_unload);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Blue DeviL // SCT");
MODULE_DESCRIPTION("IDT Parser Kernel Module");

