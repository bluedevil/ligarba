#include <stdio.h>
#include <stdint.h>     // uintxx_t burada tanımlı
#include <stdlib.h>     // EXIT_SUCCESS burada tanımlı
#include <string.h>     // strlen
#include <unistd.h>     // read open burada tanımlı
#include <fcntl.h>      // O_RDONLY burada tanımlı
#include <asm/segment.h>// KERNEL_CS vd. burada tanımlı

#define NRM  "\x1B[0m"
#define KZL  "\x1B[31m"
#define YSL  "\x1B[32m"
#define SRI  "\x1B[33m"
#define GOK  "\x1B[34m"
#define MOR  "\x1B[35m"
#define TUR  "\x1B[36m"
#define AK  "\x1B[37m"
// koyu
#define NRMK  "\x1B[1m"
#define KZLK  "\x1B[1;31m"
#define YSLK  "\x1B[1;32m"
#define SRIK  "\x1B[1;33m"
#define GOKK  "\x1B[1;34m"
#define MORK  "\x1B[1;35m"
#define TURK  "\x1B[1;36m"
#define AKK   "\x1B[1;37m"

char sym[1024];
unsigned long int tmp = 0;
/**
 * gate_struct64 -  çekirdekte bulunan bir yapıdır. Kesme tanımlayıcı tablosu bu
 * yapıya göre kurulur. Kullanıcı uzayında bu yapı tanımlanmadığı için elle tanı
 * mladık. Çekirdek uzayından bu yapının aynısını çektiğimiz için kullanıcı uzay
 * ında da bir karşılığı olmalı. Bu nedenle burada da tanımlamak gerekli.
 */
struct gate_struct64 {
    uint16_t offset_low;
    uint16_t segment;
    unsigned ist : 3, zero0 : 5, type : 5, dpl : 2, p : 1;
    uint16_t offset_middle;
    uint32_t offset_high;
    uint32_t zero1;
} __attribute__((packed)) *gate;

/**
 * baslik_bas - uygulamaya başlık bilgisi basar
 */
void baslik_bas(void)
{
    char *baslik =
    SRIK
    "                         _                        _                    \n"
    NRM
    GOKK"       .:/+++/:-`"NRM
    SRIK"       (_ ) _                   ( )                   \n"NRM
    GOKK"    `/s          o:`"NRM 
    SRIK"     | |(_)   __     _ _ _ __| |_      _ _         \n"NRM
    GOKK"   /  \\ "MORK"/\\"GOKK"  /      s-"NRM 
    SRIK"    | || | /'_ `\\ /'_` | '__) '_`\\  /'_` )     \n"NRM
    GOKK" `o  "MORK"<oddbbd"GOKK"    O    -"NRM 
    SRIK"   | || |( (_) |( (_| | |  | |_) )( (_| |        \n"NRM
    GOKK" +    "MORK"ddoodb>"GOKK"    o   o"NRM 
    SRIK"  (___|_)`\\__  |`\\__,_|_)  (_,__/'`\\__,_)      \n"NRM
    GOKK" s   "MORK"<dhqpdo"GOKK"   o     s"NRM 
    SRIK"         ( )_) |                                 \n"NRM
    GOKK" +   /  "MORK"\\/"GOKK" \\         +"NRM 
    SRIK"          \\___/'                               \n"NRM
    GOKK" .     o            s`"NRM 
    NRM" Yaylalari daglari da ligarbalar mi sardi...     \n"
    GOKK"  -s  O            o`"NRM 
    NRM"  Cekip giden sen idun da gene suc bana kaldi..   \n"
    GOKK"   `/s  o        o-"NRM 
    NRM"    Oy yaylalar yaylalar da tirpan cimen kucaklar.. \n"
    GOKK"      `-/++++/:-`"NRM 
    NRM"      Yuregi yanan var mi da benum gibi usaklar...    \n";

    char *bilgi =
    NRMK
    "         _____________________________________________________        \n"
    NRMK
    "      -+| @yazar    "MORK":"NRM
    NRMK" Blue DeviL <bluedevil@sctzine.com>      |+-     \n"
    NRMK" _   |  | @surum    "MORK":"NRM
    NRMK" 0.1.0                                   |  |   _\n"
    NRMK" _-==+--| @tarih    "MORK":"NRM
    NRMK" 24/12/2020                              |--+==-_\n"
    NRMK"     |  | @lisans   "MORK":"NRM
    NRMK" MIT                                     |  |    \n"
    NRMK"      -+| @bilgi    "MORK":"NRM
    NRMK" IDT Parser              www.sctzine.com |+-     \n"
    NRMK
    "        |_____________________________________________________|       \n"
    NRM;
    fprintf(stdout, "%s%s\n", baslik, bilgi);
    return;
}

/**
 * aygita_erisim - linux'ta her şey dosyadır mantığıyla,  aygıtımıza okuma/yazma
 * yapmadan önce onu `open` sistem çağrısıyla açıp dosya betimleyicisini döndürü
 * yoruz.
 * @aygit_yolu: açılıp dosya tanımlayıcısı döndürülecek aygıtın yolu.
 *
 * Dönüş değeri: açılan aygıtın dosya tanımlayıcısı. Hata olursa çıkılır.
 */
int aygita_erisim(char *aygit_yolu)
{
    int fd = open(aygit_yolu, O_RDWR);
    if (fd < 0){
        perror("open");
        exit(EXIT_FAILURE);
    }

    return fd;
}

/**
 * sembolu_bul - linux çekirdeği derlenirken isteğe bağlı olarak `System.map`adı
 * aldında çekirdekteki yordamların adresleri ve bunlara karşılık sembol bilgisi
 * ile bazı gerekli bilgiler tutulur.  System.map aslında bir metin  dosyasıdır.
 * `sembolu_bul`  yordamı ile çekirdeğe ait bir adresin sembol bilgisini  arayıp
 * bulursak o sembol değerini döndürüyoruz.
 * @harita_yolu: System.map dosyasının yolu
 * @addr: System.map dosyasında aratılacak adres bilgisi
 *
 * Dönüş değeri: Eğer bulunursa bulunan sembol yoksa bulunamadı bilgisi.
 */
char *sembolu_bul(char *harita_yolu,  unsigned long int addr)
{
    FILE *fp;
    char off[17];
    int bulundu_sayaci = 0;
    // tamsayı olarak gelen adres bilgisini burada karakter dizisi yapıyoruz.
    sprintf(off, "%lx", addr);

    fp = fopen(harita_yolu, "r");
    if (fp < 0) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    while (fgets(sym, 1024, fp) != NULL) {
        if (strstr(sym, off) != NULL) {
            bulundu_sayaci++;
            fclose(fp);
            // tum satırı döndürür
            //return sym;
            // sadece yordam adini dondurur.
            memmove(sym, sym+19, (strlen(sym) - 19));
            memset(sym + (strlen(sym) - 20), 0, 1); 
            return sym;
        }
    }

    fclose(fp);
    return "bulunamadi";
}

/**
 * bolum_adi - Her IDT kapısında o kapıyı karşılayacak yordama ait bilgilerden b
 * iri hangi bölümde olduğudur. `asm/segment.h` başlık dosyasında bulunan bu bil
 * gileri çekirdekten çektiğimiz yapı içerisindeki `segment_selector` bilgisiyle
 * karşılaştırarak öğrenebiliriz.
 * @seg_sel: ilgili kesmeyi karşılayan yordama ait kapıdaki `segment` bilgisi
 *
 * Dönüş değeri: Karakter dizisi olarak bölüm adı.
 */
char *bolum_adi(unsigned short int seg_sel)
{
    static char olmayan_bolum[4];
    if (seg_sel == __KERNEL_CS) {
        return ("KERNEL_CS");
    } else if (seg_sel == __KERNEL_DS) {
        return ("KERNEL_DS");
    } else if (seg_sel == __USER_CS) {
        return ("USER_CS");
    } else if (seg_sel == __USER_DS) {
        return ("USER_DS");
    } else {
      // eger bulamazsak değerini döndürelim:
      sprintf(olmayan_bolum, "%d", seg_sel);
      return (olmayan_bolum);
    }
}

/**
 * kapi_turu - her IDT kapısında o kapıyı karşılayacak yordama ait bilgiler bulu
 * nur ve bunlardan biride o kapının türünün ne olduğudur. Bazı kapılar KESME ka
 * pısı iken bazıları ÇAĞRI veya TUZAK kapısı olabilir. Bunların ayrıntıları içi
 * n intel işlemci kılavuzları incelenmelidir. Çünkü mimariye göre değişiklik gö
 * sterebilir.
 * @tur: IDT kapısında gelen ve ilgili yordama ait tür bilgisi
 *
 * Dönüş değeri: Karakter dizisi olarak gelen tur değerinin hangi kapı olduğu*/
char *kapi_turu(unsigned short int tur) 
{
    static char olmayan_tur[3];
    // tur 4 bit olabilir
    tur = tur & 0xF;
    if (tur == 5) {
        return ("GOREV KAPISI");
    } else if (tur == 12) {
        return ("CAGRI KAPISI");
    } else if (tur == 14) {
        return ("KESME KAPISI");
    } else if (tur == 15) {
        return ("TUZAK KAPISI");
    } else {
        // eğer bulamazsak tur değerini döndürelim
        sprintf(olmayan_tur, "%d", tur);
        return (olmayan_tur);
    }
}

/**
 * idt_cozumle - IDT yapısını çözümleyip ekrana basan yordam
 * @fd: aygıtın dosya tanımlayıcısı
 * @harita_yolu: System.map dosyasının yolu
 */
void idt_cozumle(int fd, char *harita_yolu)
{
    int i;
    unsigned long int low = 0;
    unsigned long int hig = 0;
    gate = (struct gate_struct64 *)malloc(0xFFF * sizeof(struct gate_struct64));

    /*write kullanmaya gerek yok artik
    printf("[+] int %d\t ", i);
    ssize_t zs = write(fd, &i, sizeof(i));
    if (zs < 0) {
        perror("write");
        exit(EXIT_FAILURE);
    }
    */   
    ssize_t sz = read(fd, gate, (0xFF * sizeof(struct gate_struct64)));
    if (sz < 0) {
        perror("read");
        exit(EXIT_FAILURE);
    }
  
    printf(
    "[int]*[      adres       ]*[ segment ]*[DPL]*[     Type     ]*"
    "[   yordam adi   ]\n"
    );
    // bölmeye başlıyalım
    // idt tablosu 256(0xFF) kapidan oluşur
    for (i = 0; i <= 0xFF; i++) {
        // hig = tutamaç adresi
        low = gate[i].offset_middle;
        low = low << 16;
        low = low | gate[i].offset_low;
        hig = gate[i].offset_high;
        hig = hig << 32;
        hig = hig | low;
        
        printf("[%-3d] %#19.16lX %11s %4i %15s     %.16s\n",
                i, hig, bolum_adi(gate[i].segment), gate[i].dpl,
                kapi_turu(gate[i].type), sembolu_bul(harita_yolu, hig)); 
    }
    return;
}

int main(int argc, char** argv)
{
    // uçbirimi temizle
    system("clear");

    // uçbirime başlık bilgilerini bas
    baslik_bas();

    // gerekli argümanlar verilmezse hata verip uygulamayı sonlandıralım
    if (argc != 3) {
        printf("[*] Kullanim:\n\t./tetik <aygit_yolu> <System.map yolu>\n");
        exit(EXIT_FAILURE);
    }

    // karakter aygıtına eriş, aç ve dosya tanımlayıcısını al
    int fd = aygita_erisim(argv[1]);

    // verilen karakter aygıtından IDT verisini al ve çözümle
    idt_cozumle(fd, argv[2]);

    // karakter aygıtını kapat
    close(fd);
    return EXIT_SUCCESS;
}

